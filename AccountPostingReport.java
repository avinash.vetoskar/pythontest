package calypsox.tk.report;

import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;
import java.util.Vector;

import com.calypso.tk.core.JDatetime;
import com.calypso.tk.core.Log;
import com.calypso.tk.report.DefaultReportOutput;
import com.calypso.tk.report.Report;
import com.calypso.tk.report.ReportOutput;
import com.calypso.tk.report.ReportRow;
import com.calypso.tk.report.ReportTemplate;
import com.calypso.tk.service.DSConnection;

import calypsox.tk.constants.CustomContants;
import calypsox.tk.service.InterfaceType;
import calypsox.tk.service.RemoteDBServices;
import calypsox.tk.service.dao.bean.AcctPostingBean;

public class AccountPostingReport extends Report implements CustomContants {

	private static final long serialVersionUID = 1L;
	public static final String REPORT_TYPE = "AccountPostingReport";
	public static final String LOG_CATEGORY = "AccountPostingReport";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ReportOutput load(Vector errorMsgs) {

		DefaultReportOutput output = new DefaultReportOutput(this);

		String reportStartDate = null;
		initDates();
		ArrayList<ReportRow> rows = new ArrayList<ReportRow>();

		DSConnection dsc = DSConnection.getDefault();

		SimpleDateFormat DATE_FORMAT1 = new SimpleDateFormat("d-MMM-yy");
		ReportTemplate h = this._reportTemplate;

		TimeZone tz = h.getTimeZone();
		if (tz == null) {
			tz = TimeZone.getDefault();
		}

		JDatetime startDate = getDateTime("StartDate", "StartPlus", "StartTenor", "startDateTime",
				tz, true);

		if (startDate != null) {
			System.out.println(DATE_FORMAT1.format(startDate));
			reportStartDate = DATE_FORMAT1.format(startDate);
		}

		RemoteDBServices dbServ = getRemoteDBServices();

		try {

			SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy");
			DecimalFormat df = new DecimalFormat("#.###################");
			df.setMinimumFractionDigits(2);
			InterfaceType type = InterfaceType.ACCOUNTING;
			String branchProcessingDate = null;
			String valueDate = null;
			String debitAmountValueDate = null;
			String creditAmountValueDate = null;
			String creationDate = null;

			Vector<AcctPostingBean> acctPostingRecs = (Vector<AcctPostingBean>) dbServ
					.getAllRecords(
							"(TRUNC(VALUE_DATE)='" + reportStartDate + "' OR "
									+ "(TRUNC(CREATION_DATE)='" + reportStartDate
									+ "' AND  TRUNC(VALUE_DATE)<'" + reportStartDate + "')) ",
							InterfaceType.ACCOUNTING);
			for (AcctPostingBean acctPostingBean : acctPostingRecs) {
				ReportRow reportRow = new ReportRow(acctPostingBean);
				branchProcessingDate = DATE_FORMAT
						.format(acctPostingBean.getBranchProcessingDate());
				valueDate = DATE_FORMAT.format(acctPostingBean.getValueDate());
				debitAmountValueDate = DATE_FORMAT.format(acctPostingBean.getDebitValueDate());
				creditAmountValueDate = DATE_FORMAT.format(acctPostingBean.getCreditValueDt());
				creationDate = DATE_FORMAT.format(acctPostingBean.getCreationDate());

				reportRow.setProperty(AccountPostingReportStyle.DEAL_SOURCE,
						acctPostingBean.getDealSrc());
				reportRow.setProperty(AccountPostingReportStyle.BRANCH_CODE,
						acctPostingBean.getBranchCode());
				reportRow.setProperty(AccountPostingReportStyle.BRANCH_PROCESSING_DATE,
						branchProcessingDate);
				reportRow.setProperty(AccountPostingReportStyle.DEAL_NO,
						acctPostingBean.getDealNo());
				reportRow.setProperty(AccountPostingReportStyle.VALUE_DATE, valueDate);
				reportRow.setProperty(AccountPostingReportStyle.PRODUCT,
						acctPostingBean.getProduct());
				reportRow.setProperty(AccountPostingReportStyle.PRODUCT_SUB_TYPE,
						acctPostingBean.getProdSubType());
				reportRow.setProperty(AccountPostingReportStyle.BOOK_NAME,
						acctPostingBean.getBookName());
				reportRow.setProperty(AccountPostingReportStyle.INTERNAL_TRADE_REF,
						acctPostingBean.getInteralRefernce());
				reportRow.setProperty(AccountPostingReportStyle.EXTERNAL_TRADE_REF,
						acctPostingBean.getExernalRefernce());
				reportRow.setProperty(AccountPostingReportStyle.TRADE_COMMENTS,
						acctPostingBean.getTradeComment());
				reportRow.setProperty(AccountPostingReportStyle.DEBIT_ACCOUNT_NO,
						acctPostingBean.getDebitAccountNumber());
				reportRow.setProperty(AccountPostingReportStyle.CREDIT_ACCOUNT_NO,
						acctPostingBean.getCreditAccoutNumber());
				reportRow.setProperty(AccountPostingReportStyle.CURRENCY,
						acctPostingBean.getCurreny());
				reportRow.setProperty(AccountPostingReportStyle.AMOUNT,
						df.format(acctPostingBean.getAmount()));
				reportRow.setProperty(AccountPostingReportStyle.SETTLEMENT_MEANS,
						acctPostingBean.getSettelementMethod());
				reportRow.setProperty(AccountPostingReportStyle.CLIENT_SHORT_NAME,
						acctPostingBean.getClientShortName());
				reportRow.setProperty(AccountPostingReportStyle.CLIENT_FULL_NAME,
						acctPostingBean.getClientFullName());
				reportRow.setProperty(AccountPostingReportStyle.POSTING_ID,
						acctPostingBean.getPostingId());
				reportRow.setProperty(AccountPostingReportStyle.REQUEST_UUID,
						acctPostingBean.getRequestUUID());
				reportRow.setProperty(AccountPostingReportStyle.TRANSACTION_TYPE,
						acctPostingBean.getTransactionType());
				reportRow.setProperty(AccountPostingReportStyle.TRANSACTION_SUB_TYPE,
						acctPostingBean.getTransactionSubType());
				reportRow.setProperty(AccountPostingReportStyle.DEBIT_ACCOUNT_ID,
						acctPostingBean.getDebitAccountId());
				reportRow.setProperty(AccountPostingReportStyle.DEBIT_CREDIT_FLAG,
						acctPostingBean.getDrCreditDebitFlg());
				reportRow.setProperty(AccountPostingReportStyle.DEBIT_CURRENCY_CODE,
						acctPostingBean.getDebitCurrencyCode());
				reportRow.setProperty(AccountPostingReportStyle.DEBIT_AMOUNT_VALUE,
						df.format(acctPostingBean.getDebitAmountValue()));
				reportRow.setProperty(AccountPostingReportStyle.DEBIT_AMOUNT_VALUE_DATE,
						debitAmountValueDate);
				reportRow.setProperty(AccountPostingReportStyle.CREDIT_ACCOUNT_ID,
						acctPostingBean.getCreditAccountId());
				reportRow.setProperty(AccountPostingReportStyle.CREDIT_DEBIT_FLAG,
						acctPostingBean.getCrCreditDebitFlg());
				reportRow.setProperty(AccountPostingReportStyle.CREDIT_CURRENCY_CODE,
						acctPostingBean.getCreditCurrencyCode());
				reportRow.setProperty(AccountPostingReportStyle.CREDIT_AMOUNT_VALUE,
						df.format(acctPostingBean.getCreditAmountValue()));
				reportRow.setProperty(AccountPostingReportStyle.CREDIT_AMOUNT_VALUE_DATE,
						creditAmountValueDate);
				reportRow.setProperty(AccountPostingReportStyle.FREE_TEXT1,
						acctPostingBean.getFreeText1());
				reportRow.setProperty(AccountPostingReportStyle.FREE_TEXT2,
						acctPostingBean.getFreeText2());
				reportRow.setProperty(AccountPostingReportStyle.PROCESSED,
						acctPostingBean.getProcessed());
				reportRow.setProperty(AccountPostingReportStyle.STATUS,
						acctPostingBean.getStatus());
				reportRow.setProperty(AccountPostingReportStyle.STATUS_DESC,
						acctPostingBean.getStatusDesc());
				reportRow.setProperty(AccountPostingReportStyle.DBT_ACT_RULE_TYPE,
						acctPostingBean.getDebitAccounttRuleType());
				reportRow.setProperty(AccountPostingReportStyle.CR_ACT_RULE_TYPE,
						acctPostingBean.getCreditAccountRuleType());
				reportRow.setProperty(AccountPostingReportStyle.CREATION_DATE, creationDate);
				reportRow.setProperty(AccountPostingReportStyle.FINACLE_SEND_DATE,
						acctPostingBean.getFinacleSentDate());
				reportRow.setProperty(AccountPostingReportStyle.FINACLE_RECEIVE_DATE,
						acctPostingBean.getFinacleReceiveDate());
				rows.add(reportRow);
			}

		} catch (RemoteException e) {
			Log.error(LOG_CATEGORY, "Error while fetching trades.", e);
		}
		for (ReportRow repRow : rows) {
			repRow.setProperty(AccountPostingReportStyle.DEAL_SOURCE, rows.size());
		}
		ReportRow[] reportRows = new ReportRow[rows.size()];
		rows.toArray(reportRows);
		output.setRows(reportRows);

		return output;

	}

	/*******************************************************************************************************************
	 * get the remote service to access the custom table.
	 * 
	 * @return DSConnection.getDefault().getRemoteService(RemoteDBServices.class);
	 */
	private RemoteDBServices getRemoteDBServices() {
		return DSConnection.getDefault().getRemoteService(RemoteDBServices.class);
	}

}
